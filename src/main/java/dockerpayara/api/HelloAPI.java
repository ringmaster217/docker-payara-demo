package dockerpayara.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;

/**
 * Created by bwelker on 11/2/16.
 */
@Path("hello")
public class HelloAPI {

    @GET
    public Response getHello(){
        return Response.ok().entity("Hello! - " + LocalDateTime.now().toString()).build();
    }
}
