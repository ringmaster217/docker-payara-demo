# Payara/Docker demo

View the images/ folder for examples of `Dockerfile`s.

Use maven to compile the war file and build a docker image.
```
#!bash

mvn clean package;
docker run -p 8080:8080 docker-payara-demo
```

To access the resulting hello world service:
```
#!http

GET http://localhost:8080/dockerpayara/hello HTTP/1.1
```